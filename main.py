import socket

server_socket = socket.socket(
    socket.AF_INET,
    socket.SOCK_STREAM
)

server_socket.bind(
    ('localhost', 5000)
)

server_socket.listen()
print("Server is listening")

while True:
    user_socket, addr = server_socket.accept()
    print(f"User {user_socket} connected")

    user_socket.send('Connected'.encode('utf-8'))
    
    data = user_socket.recv(1024)

    print(data.decode('utf-8'))